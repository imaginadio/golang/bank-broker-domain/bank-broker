// Bank broker domain.
//
//     Schemes: http
//     Version: 0.1
//
//     Consumes:
//     - application/json
//
//     Produces:
//     - application/json
//
// swagger:meta
package main

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"github.com/joho/godotenv"
	"github.com/spf13/viper"
	bankbroker "gitlab.com/imaginadio/golang/bank-broker-domain/bank-broker"
	"gitlab.com/imaginadio/golang/bank-broker-domain/bank-broker/pack/communication/applapi"
	"gitlab.com/imaginadio/golang/bank-broker-domain/bank-broker/pack/communication/applqueue"
	"gitlab.com/imaginadio/golang/bank-broker-domain/bank-broker/pack/handler"
	"gitlab.com/imaginadio/golang/bank-broker-domain/bank-broker/pack/repository"
	"gitlab.com/imaginadio/golang/bank-broker-domain/bank-broker/pack/service"
	"go.uber.org/zap"
)

const localhost = "localhost"

func main() {
	// init logger
	logger := initLogger()
	defer logger.Sync()

	undo := zap.ReplaceGlobals(logger)
	defer undo()

	// init configs
	if err := initConfig(); err != nil {
		zap.L().Fatal("Error while reading config file", zap.String("msg", err.Error()))
	}

	dbHost := viper.GetString("db.host")
	messageQueueHost := viper.GetString("mbroker.host")
	bankApiHost := viper.GetString("bankapi.url")

	if os.Getenv("GIN_MODE") != "release" {
		dbHost = localhost
		messageQueueHost = localhost
		bankApiHost = "http://localhost"
	}

	// init database
	db, err := repository.NewPostgreDB(repository.Config{
		Host:     dbHost,
		Port:     viper.GetString("db.port"),
		Username: viper.GetString("db.username"),
		Password: os.Getenv("POSTGRES_PASSWORD"),
		DBName:   viper.GetString("db.dbname"),
		SSLMode:  viper.GetString("db.sslmode"),
	})

	if err != nil {
		zap.L().Fatal("Error while connecting to DB", zap.String("msg", err.Error()))
	}
	defer db.Close()

	// init message broker
	queueConnection, err := applqueue.NewConnection(applqueue.Config{
		Host:     messageQueueHost,
		Port:     viper.GetString("mbroker.port"),
		User:     viper.GetString("mbroker.user"),
		Password: os.Getenv("RABBIT_PASSWORD"),
	})
	if err != nil {
		zap.L().Fatal("Error while connecting to message broker", zap.String("msg", err.Error()))
	}
	defer queueConnection.Close()

	amqpChannel, err := queueConnection.Channel()
	if err != nil {
		zap.L().Fatal("Error while creating broker channel", zap.String("msg", err.Error()))
	}
	defer amqpChannel.Close()

	applqueue.DeclareQueues(amqpChannel)

	repo := repository.NewRepository(db)
	api := applapi.NewApplicationApi(
		applapi.BankPartnerApiConfig{
			Url:  bankApiHost,
			Port: viper.GetString("bankapi.port"),
		},
		&http.Client{},
	)
	queue := applqueue.NewApplicationQueue(amqpChannel)
	services := service.NewService(repo, api, queue)
	handlers := handler.NewHandler(services)
	srv := new(bankbroker.Server)
	defer srv.Shutdown(context.Background())

	// init incoming message broker queue
	handlers.InitMessageQueueConsumer(amqpChannel)

	// init gin handlers
	go func() {
		if err := srv.Run(viper.GetString("port"), handlers.InitRoutes()); err != nil && err != http.ErrServerClosed {
			zap.L().Fatal("Error occured while running http server", zap.String("msg", err.Error()))
		}
	}()

	zap.L().Info("App started")

	quit := make(chan os.Signal)
	signal.Notify(quit, syscall.SIGTERM, syscall.SIGINT)
	<-quit

	zap.L().Info("App shutting down")
}

func initConfig() error {
	godotenv.Load()
	viper.AddConfigPath("configs")
	viper.SetConfigName("config")
	return viper.ReadInConfig()
}

func initLogger() *zap.Logger {
	logger, err := zap.NewProduction()
	if err != nil {
		log.Fatalf("Error while init logger: %s", err.Error())
	}
	return logger
}
