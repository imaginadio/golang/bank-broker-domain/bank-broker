FROM golang:1.15-alpine as build-stage
WORKDIR /app/bank-broker-app
COPY go.mod .
COPY go.sum .
RUN go mod download
COPY . .
RUN go build -o ./out/bank-broker-app ./cmd

FROM alpine:3.12.3 as production-stage
ENV GIN_MODE=release

WORKDIR /app

COPY --from=build-stage /app/bank-broker-app/out/bank-broker-app .
COPY configs ./configs
COPY swaggerui ./swaggerui
EXPOSE 8080
ENTRYPOINT [ "/app/bank-broker-app" ]
