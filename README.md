# Bank Broker

Application consist of 2 services: bank broker and application polling service.
Bank broker get API requests from users, send it to bank partner service and store in **PostgreSQL** database. If the application has a non-final status, then it sent to polling service through the **RabbitMQ**. This service periodically polls the service of the partner bank and update application status if it changes.

## Installation
docker-compose starts 5 services. This ports must be free to start:
8000
8080
5432
5672

Commands to install:

For windows:
```
SET POSTGRES_PASSWORD=passwordexample
SET RABBIT_PASSWORD=guest
docker-compose up
```

For linux/macos:
```
export POSTGRES_PASSWORD=passwordexample
export RABBIT_PASSWORD=guest
docker-compose up
```

## Testing
Unit tests starting by command:
```
go test -v ./...
```

## Documentation
Documentation was generated by go-swagger.
After starting service API documentation will be available [here](http://localhost:8080/swaggerui)