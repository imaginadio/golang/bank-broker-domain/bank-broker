package bankbroker

type Application struct {
	Id        string `json:"id" db:"id"`
	FirstName string `json:"first_name" db:"first_name"`
	LastName  string `json:"last_name" db:"last_name"`
	Status    string `json:"status" db:"status"`
}

type ApplicationInput struct {
	Id        string `json:"id" db:"id" binding:"required"`
	FirstName string `json:"first_name" db:"first_name"`
	LastName  string `json:"last_name" db:"last_name"`
}

// Application input fields
// swagger:parameters createApplication
type applicationInputWrapper struct {
	// in:body
	Body ApplicationInput
}

type ApplicationJobResponse struct {
	Id            string `json:"id"`
	ApplicationId string `json:"application_id" db:"application_id"`
	Status        string `json:"status" db:"status"`
}
