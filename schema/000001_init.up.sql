CREATE TABLE IF NOT EXISTS applications
(
    id uuid primary key,
    first_name varchar(255),
    last_name varchar(255),
    status varchar(50)
);
