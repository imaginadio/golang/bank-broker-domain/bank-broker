package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"
	bankbroker "gitlab.com/imaginadio/golang/bank-broker-domain/bank-broker"
	"go.uber.org/zap"
)

// swagger:route POST /api/applications application createApplication
//
// Create customer application.
//
//     Consumes:
//     - application/json
//
//     Produces:
//     - application/json
//
//     Schemes: http
//
//     Responses:
//       200: okWithResult
//       400: defaultError
//       500: defaultError
func (h *Handler) createApplication(c *gin.Context) {
	var input bankbroker.ApplicationInput
	if err := c.BindJSON(&input); err != nil {
		newErrorResponse(c, http.StatusBadRequest, "Invalid input body")
		return
	}

	if err := h.services.Application.CreateApplication(input); err != nil {
		newErrorResponse(c, http.StatusInternalServerError, err.Error())
		return
	}

	c.JSON(http.StatusOK, okResponse{Result: "Application created"})
}

// GetApplications query parameters (need at least one of them)
// swagger:parameters getApplications
type getApplicationsParametersWrapper struct {
	// in:query
	// example "8a3045aa-9ee9-4fd6-a8e7-ed031c8ff102"
	Id string `json:"id"`
	// in:query
	// example "pending"
	Status string `json:"status"`
}

// swagger:route GET /api/applications application getApplications
//
// Get application by id or all applications by status.
//
// Consume parameters by priority:
// 1) id
// 2) status (will be ignored if "id" is provided)
//
//     Produces:
//     - application/json
//
//     Schemes: http
//
//     Responses:
//       200: applicationsResponse
//       400: defaultError
//       500: defaultError
func (h *Handler) getApplications(c *gin.Context) {
	appId := c.Query("id")
	status := c.Query("status")

	if appId != "" {
		app, err := h.services.Application.GetApplicationById(appId)
		if err != nil {
			zap.L().Error("GetApplicationById error", zap.String("msg", err.Error()))
			newErrorResponse(c, http.StatusInternalServerError, "Invalid id")
			return
		}

		c.JSON(http.StatusOK, applicationsResponse{
			Result: []bankbroker.Application{app},
		})
	} else if status != "" {
		apps, err := h.services.Application.GetApplicationsByStatus(status)
		if err != nil {
			newErrorResponse(c, http.StatusInternalServerError, err.Error())
			return
		}

		c.JSON(http.StatusOK, applicationsResponse{
			Result: apps,
		})
	} else {
		newErrorResponse(c, http.StatusBadRequest, "You must provide id or status query parameters")
		return
	}
}
