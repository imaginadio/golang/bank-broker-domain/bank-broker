package handler

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/imaginadio/golang/bank-broker-domain/bank-broker/pack/service"
)

type Handler struct {
	services *service.Service
}

func NewHandler(services *service.Service) *Handler {
	return &Handler{services: services}
}

func (h *Handler) InitRoutes() *gin.Engine {
	router := gin.New()

	router.Static("/swaggerui", "swaggerui")

	api := router.Group("/api")
	{
		apps := api.Group("/applications")
		{
			apps.POST("", h.createApplication)
			apps.GET("", h.getApplications)
		}
	}

	return router
}
