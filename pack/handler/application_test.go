package handler

import (
	"bytes"
	"errors"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/assert/v2"
	"github.com/golang/mock/gomock"
	bankbroker "gitlab.com/imaginadio/golang/bank-broker-domain/bank-broker"
	"gitlab.com/imaginadio/golang/bank-broker-domain/bank-broker/pack/service"
	mock_service "gitlab.com/imaginadio/golang/bank-broker-domain/bank-broker/pack/service/mocks"
)

func TestHandler_createApplication(t *testing.T) {
	type mockBehavior func(s *mock_service.MockApplication, app bankbroker.ApplicationInput)

	testTable := []struct {
		name                 string
		inputBody            string
		inputApplication     bankbroker.ApplicationInput
		mockBehavior         mockBehavior
		expectedStatusCode   int
		expectedResponseBody string
	}{
		{
			name:      "OK",
			inputBody: `{"id":"2a1e344b-fd1f-42a0-965e-fae763ed6c7a", "first_name":"test", "last_name":"test"}`,
			inputApplication: bankbroker.ApplicationInput{
				Id:        "2a1e344b-fd1f-42a0-965e-fae763ed6c7a",
				FirstName: "test",
				LastName:  "test",
			},
			mockBehavior: func(s *mock_service.MockApplication, app bankbroker.ApplicationInput) {
				s.EXPECT().CreateApplication(app).Return(nil)
			},
			expectedStatusCode:   http.StatusOK,
			expectedResponseBody: `{"result":"Application created"}`,
		},
		{
			name:                 "Empty fields",
			inputBody:            `{"first_name":"test", "last_name":"test"}`,
			mockBehavior:         func(s *mock_service.MockApplication, app bankbroker.ApplicationInput) {},
			expectedStatusCode:   http.StatusBadRequest,
			expectedResponseBody: `{"message":"Invalid input body"}`,
		},
		{
			name:      "Internal server error",
			inputBody: `{"id":"2a1e344b-fd1f-42a0-965e-fae763ed6c7a", "first_name":"test", "last_name":"test"}`,
			inputApplication: bankbroker.ApplicationInput{
				Id:        "2a1e344b-fd1f-42a0-965e-fae763ed6c7a",
				FirstName: "test",
				LastName:  "test",
			},
			mockBehavior: func(s *mock_service.MockApplication, app bankbroker.ApplicationInput) {
				s.EXPECT().CreateApplication(app).Return(errors.New("internal server error"))
			},
			expectedStatusCode:   http.StatusInternalServerError,
			expectedResponseBody: `{"message":"internal server error"}`,
		},
	}

	for _, tc := range testTable {
		t.Run(tc.name, func(t *testing.T) {
			// init dependencies
			c := gomock.NewController(t)
			defer c.Finish()

			appl := mock_service.NewMockApplication(c)
			tc.mockBehavior(appl, tc.inputApplication)

			services := &service.Service{Application: appl}
			handler := NewHandler(services)

			// start test server
			r := gin.New()
			r.POST("/application", handler.createApplication)

			// test request
			w := httptest.NewRecorder()
			req := httptest.NewRequest(http.MethodPost, "/application", bytes.NewBufferString(tc.inputBody))

			// perform request
			r.ServeHTTP(w, req)

			// assert
			assert.Equal(t, tc.expectedStatusCode, w.Code)
			assert.Equal(t, tc.expectedResponseBody, w.Body.String())
		})
	}
}

func TestHandler_getApplications(t *testing.T) {
	type mockBehavior func(s *mock_service.MockApplication, qparam string)

	testTable := []struct {
		name                 string
		inputQueryParams     string
		param                string
		mockBehavior         mockBehavior
		expectedStatusCode   int
		expectedResponseBody string
	}{
		{
			name:             "id OK",
			inputQueryParams: "id=c8e7a8ae-930d-4d3c-83f7-a938ea3be9f4",
			param:            "c8e7a8ae-930d-4d3c-83f7-a938ea3be9f4",
			mockBehavior: func(s *mock_service.MockApplication, qparam string) {
				s.EXPECT().GetApplicationById(qparam).Return(bankbroker.Application{
					Id:        "c8e7a8ae-930d-4d3c-83f7-a938ea3be9f4",
					FirstName: "test",
					LastName:  "test",
					Status:    "pending",
				}, nil)
			},
			expectedStatusCode:   http.StatusOK,
			expectedResponseBody: `{"result":[{"id":"c8e7a8ae-930d-4d3c-83f7-a938ea3be9f4","first_name":"test","last_name":"test","status":"pending"}]}`,
		},
		{
			name:             "id invalid or not found",
			inputQueryParams: "id=123",
			param:            "123",
			mockBehavior: func(s *mock_service.MockApplication, qparam string) {
				s.EXPECT().GetApplicationById(qparam).Return(bankbroker.Application{}, errors.New("appplication find error"))
			},
			expectedStatusCode:   http.StatusInternalServerError,
			expectedResponseBody: `{"message":"Invalid id"}`,
		},
		{
			name:             "status OK",
			inputQueryParams: "status=pending",
			param:            "pending",
			mockBehavior: func(s *mock_service.MockApplication, qparam string) {
				s.EXPECT().GetApplicationsByStatus(qparam).Return([]bankbroker.Application{
					{
						Id:        "c8e7a8ae-930d-4d3c-83f7-a938ea3be9f4",
						FirstName: "test",
						LastName:  "test",
						Status:    "pending",
					},
					{
						Id:        "f08999b3-f991-4477-a487-191d8ca78655",
						FirstName: "test",
						LastName:  "test",
						Status:    "pending",
					},
				}, nil)
			},
			expectedStatusCode:   http.StatusOK,
			expectedResponseBody: `{"result":[{"id":"c8e7a8ae-930d-4d3c-83f7-a938ea3be9f4","first_name":"test","last_name":"test","status":"pending"},{"id":"f08999b3-f991-4477-a487-191d8ca78655","first_name":"test","last_name":"test","status":"pending"}]}`,
		},
		{
			name:             "status not found",
			inputQueryParams: "status=error",
			param:            "error",
			mockBehavior: func(s *mock_service.MockApplication, qparam string) {
				s.EXPECT().GetApplicationsByStatus(qparam).Return(nil, nil)
			},
			expectedStatusCode:   http.StatusOK,
			expectedResponseBody: `{"result":null}`,
		},
		{
			name:             "id with status OK (status ignored)",
			inputQueryParams: "id=c8e7a8ae-930d-4d3c-83f7-a938ea3be9f4&status=pending",
			param:            "c8e7a8ae-930d-4d3c-83f7-a938ea3be9f4",
			mockBehavior: func(s *mock_service.MockApplication, qparam string) {
				s.EXPECT().GetApplicationById(qparam).Return(bankbroker.Application{
					Id:        "c8e7a8ae-930d-4d3c-83f7-a938ea3be9f4",
					FirstName: "test",
					LastName:  "test",
					Status:    "pending",
				}, nil)
			},
			expectedStatusCode:   http.StatusOK,
			expectedResponseBody: `{"result":[{"id":"c8e7a8ae-930d-4d3c-83f7-a938ea3be9f4","first_name":"test","last_name":"test","status":"pending"}]}`,
		},
		{
			name:                 "query params not passed",
			mockBehavior:         func(s *mock_service.MockApplication, qparam string) {},
			expectedStatusCode:   http.StatusBadRequest,
			expectedResponseBody: `{"message":"You must provide id or status query parameters"}`,
		},
		{
			name:                 "empty parameters passed",
			inputQueryParams:     "id=&status=",
			mockBehavior:         func(s *mock_service.MockApplication, qparam string) {},
			expectedStatusCode:   http.StatusBadRequest,
			expectedResponseBody: `{"message":"You must provide id or status query parameters"}`,
		},
	}

	for _, tc := range testTable {
		t.Run(tc.name, func(t *testing.T) {
			c := gomock.NewController(t)
			defer c.Finish()

			appl := mock_service.NewMockApplication(c)
			tc.mockBehavior(appl, tc.param)

			services := &service.Service{Application: appl}
			handler := NewHandler(services)

			r := gin.New()

			r.GET("/applications", handler.getApplications)

			w := httptest.NewRecorder()
			req := httptest.NewRequest(http.MethodGet, fmt.Sprintf("/applications?%s", tc.inputQueryParams), nil)

			r.ServeHTTP(w, req)

			assert.Equal(t, tc.expectedStatusCode, w.Code)
			assert.Equal(t, tc.expectedResponseBody, w.Body.String())
		})
	}
}
