package handler

import (
	"github.com/gin-gonic/gin"
	bankbroker "gitlab.com/imaginadio/golang/bank-broker-domain/bank-broker"
	"go.uber.org/zap"
)

// Response on any type of error.
// swagger:response defaultError
type errorResponse struct {
	// Required: true
	Message string `json:"message"`
}

// Ok response with returning value.
// swagger:response okWithResult
type okResponse struct {
	Result string `json:"result"`
}

// Response with applications list.
// swagger:response applicationsResponse
type applicationsResponse struct {
	// in: body
	Result []bankbroker.Application `json:"result"`
}

func newErrorResponse(c *gin.Context, statusCode int, message string) {
	zap.L().Error(message, zap.Int("statuscode", statusCode))
	c.AbortWithStatusJSON(statusCode, errorResponse{message})
}
