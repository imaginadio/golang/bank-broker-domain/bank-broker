package handler

import (
	"encoding/json"

	"github.com/spf13/viper"
	"github.com/streadway/amqp"
	bankbroker "gitlab.com/imaginadio/golang/bank-broker-domain/bank-broker"
	"gitlab.com/imaginadio/golang/bank-broker-domain/bank-broker/pack/communication/applqueue"
	"go.uber.org/zap"
)

const (
	consumer_name = "BankBroker"
)

func (h *Handler) processAmqpMessage(d amqp.Delivery) {
	var response bankbroker.ApplicationJobResponse
	if err := json.Unmarshal(d.Body, &response); err != nil {
		zap.L().Error("Error while unmarshal broker body", zap.String("msg", err.Error()))
		// message corrupted, reject it without requeue
		d.Reject(false)
		return
	}

	// write result to database
	if err := h.services.Application.UpdateApplication(response); err != nil {
		zap.L().Error("Error while updating application in database", zap.String("msg", err.Error()))
		d.Nack(false, true)
		return
	}

	d.Ack(false)
}

func (h *Handler) InitMessageQueueConsumer(ch *amqp.Channel) {
	if err := ch.Qos(viper.GetInt("mbroker.qos.prefetch_count"), viper.GetInt("mbroker.qos.prefetch_size"), false); err != nil {
		zap.S().Fatalf("Error on setting Qos: %s", err.Error())
	}

	readyApps, err := ch.Consume(
		applqueue.Ready_applications_queue,
		consumer_name,
		false,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		zap.S().Fatalf("Error on consume to queue %s: %s", applqueue.Ready_applications_queue, err.Error())
	}

	go func() {
		for d := range readyApps {
			go h.processAmqpMessage(d)
		}
	}()
}
