package applapi

import (
	"net/http"

	bankbroker "gitlab.com/imaginadio/golang/bank-broker-domain/bank-broker"
)

type Application interface {
	CreateApplication(app bankbroker.ApplicationInput) (bankbroker.Application, error)
}

type ApplicationApi struct {
	Application
}

func NewApplicationApi(config BankPartnerApiConfig, client *http.Client) *ApplicationApi {
	return &ApplicationApi{
		Application: NewBankPartnerApi(config, client),
	}
}
