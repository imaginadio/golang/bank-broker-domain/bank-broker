package applapi

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"

	bankbroker "gitlab.com/imaginadio/golang/bank-broker-domain/bank-broker"
	"go.uber.org/zap"
)

const (
	url_api_app  = "/api/applications"
	content_type = "application/json"
)

type BankPartnerApi struct {
	config BankPartnerApiConfig
	client *http.Client
}

type BankPartnerApiConfig struct {
	Url  string
	Port string
}

func NewBankPartnerApi(config BankPartnerApiConfig, client *http.Client) *BankPartnerApi {
	return &BankPartnerApi{
		config: config,
		client: client,
	}
}

func (c *BankPartnerApi) CreateApplication(app bankbroker.ApplicationInput) (bankbroker.Application, error) {
	var result bankbroker.Application
	appBytes, err := json.Marshal(app)
	if err != nil {
		return result, err
	}

	url := fmt.Sprintf("%s:%s%s", c.config.Url, c.config.Port, url_api_app)
	req, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(appBytes))
	req.Header.Set("Content-Type", "application/json")

	resp, err := c.client.Do(req)
	if err != nil {
		return result, err
	}
	defer resp.Body.Close()

	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return result, err
	}

	zap.L().Info("Bank partner api return result", zap.String("url", url), zap.ByteString("response", respBody))

	if err := json.Unmarshal(respBody, &result); err != nil {
		return result, err
	}

	if result.Id == "" {
		return result, errors.New(string(respBody))
	}

	return result, nil
}
