package applqueue

import (
	"fmt"

	"github.com/streadway/amqp"
)

const (
	Pending_applications_queue = "PendingApplicationsQueue"
	Ready_applications_queue   = "ReadyApplicationsQueue"
)

type Config struct {
	User     string
	Password string
	Host     string
	Port     string
}

func NewConnection(cfg Config) (*amqp.Connection, error) {
	return amqp.Dial(fmt.Sprintf("amqp://%s:%s@%s:%s/", cfg.User, cfg.Password, cfg.Host, cfg.Port))
}

func DeclareQueues(ch *amqp.Channel) error {
	_, err := ch.QueueDeclare(
		Pending_applications_queue,
		true,
		false,
		false,
		false,
		nil,
	)

	if err != nil {
		return err
	}

	_, err = ch.QueueDeclare(
		Ready_applications_queue,
		true,
		false,
		false,
		false,
		nil,
	)

	if err != nil {
		return err
	}

	return nil
}

type RabbitQueue struct {
	ch *amqp.Channel
}

func NewRabbitQueue(ch *amqp.Channel) *RabbitQueue {
	return &RabbitQueue{ch: ch}
}

func (q *RabbitQueue) SendPendingToQueue(id string) error {
	return q.ch.Publish(
		"",
		Pending_applications_queue,
		false,
		false,
		amqp.Publishing{
			DeliveryMode: amqp.Persistent,
			ContentType:  "text/plain",
			Body:         []byte(id),
		},
	)
}
