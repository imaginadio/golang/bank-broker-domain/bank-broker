package applqueue

import (
	"github.com/streadway/amqp"
)

type Application interface {
	SendPendingToQueue(id string) error
}

type ApplicationQueue struct {
	Application
}

func NewApplicationQueue(ch *amqp.Channel) *ApplicationQueue {
	return &ApplicationQueue{
		Application: NewRabbitQueue(ch),
	}
}
