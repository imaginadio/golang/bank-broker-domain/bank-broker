package repository

import (
	"errors"
	"fmt"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
	bankbroker "gitlab.com/imaginadio/golang/bank-broker-domain/bank-broker"
	"go.uber.org/zap"
)

func TestApplicationPg_CreateApplication(t *testing.T) {
	mockDB, mock, err := sqlmock.New()
	if err != nil {
		zap.S().Fatalf("error while creating mock db: %s", err.Error())
	}
	defer mockDB.Close()

	db := sqlx.NewDb(mockDB, "sqlmock")
	r := NewApplicationPg(db)

	type mockBehaviour func(app bankbroker.Application)

	testTable := []struct {
		name          string
		mockBehaviour mockBehaviour
		app           bankbroker.Application
		expectError   bool
	}{
		{
			name: "OK",
			app: bankbroker.Application{
				Id:        "89d92caa-66b7-4551-a94c-64cdd9254e9b",
				FirstName: "test",
				LastName:  "test",
				Status:    "pending",
			},
			mockBehaviour: func(app bankbroker.Application) {
				mock.ExpectExec(fmt.Sprintf("INSERT INTO %s", tapplications)).WithArgs(app.Id, app.FirstName, app.LastName, app.Status).WillReturnResult(sqlmock.NewResult(1, 1))
			},
			expectError: false,
		},
		{
			name: "Empty fields",
			app: bankbroker.Application{
				Id:        "",
				FirstName: "test",
				LastName:  "test",
				Status:    "pending",
			},
			mockBehaviour: func(app bankbroker.Application) {
				mock.ExpectExec(fmt.Sprintf("INSERT INTO %s", tapplications)).WithArgs(app.Id, app.FirstName, app.LastName, app.Status).WillReturnError(errors.New("id constraint"))
			},
			expectError: true,
		},
	}

	for _, tc := range testTable {
		t.Run(tc.name, func(t *testing.T) {
			tc.mockBehaviour(tc.app)
			err := r.CreateApplication(tc.app)

			if tc.expectError {
				assert.Error(t, err)
			} else {
				assert.NoError(t, err)
			}

			assert.NoError(t, mock.ExpectationsWereMet())
		})
	}

}

func TestApplicationPg_UpdateApplication(t *testing.T) {
	mockDB, mock, err := sqlmock.New()
	if err != nil {
		zap.S().Fatalf("error while creating mock db: %s", err.Error())
	}
	defer mockDB.Close()

	db := sqlx.NewDb(mockDB, "sqlmock")
	r := NewApplicationPg(db)

	type mockBehaviour func(app bankbroker.ApplicationJobResponse)

	testTable := []struct {
		name          string
		mockBehaviour mockBehaviour
		app           bankbroker.ApplicationJobResponse
		expectError   bool
	}{
		{
			name: "OK",
			app: bankbroker.ApplicationJobResponse{
				Id:            "89d92caa-66b7-4551-a94c-64cdd9254e9b",
				ApplicationId: "0a680520-cfcd-4ae9-b728-20311e009337",
				Status:        "pending",
			},
			mockBehaviour: func(app bankbroker.ApplicationJobResponse) {
				mock.ExpectExec(fmt.Sprintf("UPDATE %s", tapplications)).WithArgs(app.Status, app.ApplicationId).WillReturnResult(sqlmock.NewResult(1, 1))
			},
			expectError: false,
		},
		{
			name: "Empty fields",
			app: bankbroker.ApplicationJobResponse{
				Id:            "",
				ApplicationId: "",
				Status:        "pending",
			},
			mockBehaviour: func(app bankbroker.ApplicationJobResponse) {
				mock.ExpectExec(fmt.Sprintf("UPDATE %s", tapplications)).WithArgs(app.Status, app.ApplicationId).WillReturnError(errors.New("id not found"))
			},
			expectError: true,
		},
	}

	for _, tc := range testTable {
		t.Run(tc.name, func(t *testing.T) {
			tc.mockBehaviour(tc.app)
			err := r.UpdateApplication(tc.app)

			if tc.expectError {
				assert.Error(t, err)
			} else {
				assert.NoError(t, err)
			}

			assert.NoError(t, mock.ExpectationsWereMet())
		})
	}

}

func TestApplicationPg_GetApplicationById(t *testing.T) {
	mockDB, mock, err := sqlmock.New()
	if err != nil {
		zap.S().Fatalf("error while creating mock db: %s", err.Error())
	}
	defer mockDB.Close()

	db := sqlx.NewDb(mockDB, "sqlmock")
	r := NewApplicationPg(db)

	type mockBehaviour func(id string)

	testTable := []struct {
		name          string
		id            string
		mockBehaviour mockBehaviour
		expectResult  bankbroker.Application
		expectError   bool
	}{
		{
			name: "OK",
			id:   "72dbb3da-b16a-42d5-9472-40a8779718d2",
			mockBehaviour: func(id string) {
				rows := sqlmock.NewRows([]string{"id", "first_name", "last_name", "status"}).
					AddRow("72dbb3da-b16a-42d5-9472-40a8779718d2", "test", "test", "pending")
				mock.ExpectQuery(fmt.Sprintf("SELECT (.+) FROM %s", tapplications)).
					WithArgs(id).
					WillReturnRows(rows)
			},
			expectResult: bankbroker.Application{
				Id:        "72dbb3da-b16a-42d5-9472-40a8779718d2",
				FirstName: "test",
				LastName:  "test",
				Status:    "pending",
			},
			expectError: false,
		},
		{
			name: "no data found",
			id:   "72dbb3da-b16a-42d5-9472-40a8779718d3",
			mockBehaviour: func(id string) {
				rows := sqlmock.NewRows([]string{"id", "first_name", "last_name", "status"})
				mock.ExpectQuery(fmt.Sprintf("SELECT (.+) FROM %s", tapplications)).
					WithArgs(id).
					WillReturnRows(rows)
			},
			expectError: true,
		},
		{
			name: "invalid uuid",
			id:   "123",
			mockBehaviour: func(id string) {
				mock.ExpectQuery(fmt.Sprintf("SELECT (.+) FROM %s", tapplications)).
					WithArgs(id).
					WillReturnError(errors.New("invalid uuid"))
			},
			expectError: true,
		},
	}

	for _, tc := range testTable {
		t.Run(tc.name, func(t *testing.T) {
			tc.mockBehaviour(tc.id)
			app, err := r.GetApplicationById(tc.id)

			if tc.expectError {
				assert.Error(t, err)
			} else {
				assert.NoError(t, err)
				assert.Equal(t, tc.expectResult, app)
			}

			assert.NoError(t, mock.ExpectationsWereMet())
		})
	}

}

func TestApplicationPg_GetApplicationsByStatus(t *testing.T) {
	mockDB, mock, err := sqlmock.New()
	if err != nil {
		zap.S().Fatalf("error while creating mock db: %s", err.Error())
	}
	defer mockDB.Close()

	db := sqlx.NewDb(mockDB, "sqlmock")
	r := NewApplicationPg(db)

	type mockBehaviour func(status string)

	testTable := []struct {
		name          string
		status        string
		mockBehaviour mockBehaviour
		expectResult  []bankbroker.Application
		expectError   bool
	}{
		{
			name:   "OK",
			status: "pending",
			mockBehaviour: func(status string) {
				rows := sqlmock.NewRows([]string{"id", "first_name", "last_name", "status"}).
					AddRow("72dbb3da-b16a-42d5-9472-40a8779718d2", "test", "test", "pending").
					AddRow("72dbb3da-b16a-42d5-9472-40a8779718d3", "test", "test", "pending")
				mock.ExpectQuery(fmt.Sprintf("SELECT (.+) FROM %s", tapplications)).
					WithArgs(status).
					WillReturnRows(rows)
			},
			expectResult: []bankbroker.Application{
				{
					Id:        "72dbb3da-b16a-42d5-9472-40a8779718d2",
					FirstName: "test",
					LastName:  "test",
					Status:    "pending",
				},
				{
					Id:        "72dbb3da-b16a-42d5-9472-40a8779718d3",
					FirstName: "test",
					LastName:  "test",
					Status:    "pending",
				},
			},
			expectError: false,
		},
		{
			name:   "no data found",
			status: "fake",
			mockBehaviour: func(status string) {
				rows := sqlmock.NewRows([]string{"id", "first_name", "last_name", "status"})
				mock.ExpectQuery(fmt.Sprintf("SELECT (.+) FROM %s", tapplications)).
					WithArgs(status).
					WillReturnRows(rows)
			},
			expectError: false,
		},
	}

	for _, tc := range testTable {
		t.Run(tc.name, func(t *testing.T) {
			tc.mockBehaviour(tc.status)
			apps, err := r.GetApplicationsByStatus(tc.status)

			if tc.expectError {
				assert.Error(t, err)
			} else {
				assert.NoError(t, err)
				assert.Equal(t, tc.expectResult, apps)
			}

			assert.NoError(t, mock.ExpectationsWereMet())
		})
	}

}
