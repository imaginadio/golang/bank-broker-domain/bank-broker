package repository

import (
	"fmt"

	"github.com/jmoiron/sqlx"
	bankbroker "gitlab.com/imaginadio/golang/bank-broker-domain/bank-broker"
)

type ApplicationPg struct {
	db *sqlx.DB
}

func NewApplicationPg(db *sqlx.DB) *ApplicationPg {
	return &ApplicationPg{db: db}
}

func (r *ApplicationPg) CreateApplication(app bankbroker.Application) error {
	query := fmt.Sprintf(`INSERT INTO %s (id, first_name, last_name, status)
												VALUES (:id, :first_name, :last_name, :status)`, tapplications)

	_, err := r.db.NamedExec(query, app)
	return err
}

func (r *ApplicationPg) UpdateApplication(app bankbroker.ApplicationJobResponse) error {
	query := fmt.Sprintf(`UPDATE %s
                           SET status = :status
                         WHERE id = :application_id`, tapplications)

	_, err := r.db.NamedExec(query, app)
	return err
}

func (r *ApplicationPg) GetApplicationById(id string) (bankbroker.Application, error) {
	var application bankbroker.Application
	query := fmt.Sprintf(`SELECT a.id, a.first_name, a.last_name, a.status
	                        FROM %s a
                         WHERE a.id = $1`, tapplications)
	err := r.db.Get(&application, query, id)

	return application, err
}

func (r *ApplicationPg) GetApplicationsByStatus(status string) ([]bankbroker.Application, error) {
	var applications []bankbroker.Application
	query := fmt.Sprintf(`SELECT a.id, a.first_name, a.last_name, a.status
	                        FROM %s a
                         WHERE a.status = $1`, tapplications)
	err := r.db.Select(&applications, query, status)

	return applications, err
}
