package repository

import (
	"github.com/jmoiron/sqlx"
	bankbroker "gitlab.com/imaginadio/golang/bank-broker-domain/bank-broker"
)

type Application interface {
	CreateApplication(app bankbroker.Application) error
	UpdateApplication(app bankbroker.ApplicationJobResponse) error
	GetApplicationById(id string) (bankbroker.Application, error)
	GetApplicationsByStatus(status string) ([]bankbroker.Application, error)
}

type Repository struct {
	Application
}

func NewRepository(db *sqlx.DB) *Repository {
	return &Repository{
		Application: NewApplicationPg(db),
	}
}
