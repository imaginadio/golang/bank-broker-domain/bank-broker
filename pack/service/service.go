package service

import (
	bankbroker "gitlab.com/imaginadio/golang/bank-broker-domain/bank-broker"
	"gitlab.com/imaginadio/golang/bank-broker-domain/bank-broker/pack/communication/applapi"
	"gitlab.com/imaginadio/golang/bank-broker-domain/bank-broker/pack/communication/applqueue"
	"gitlab.com/imaginadio/golang/bank-broker-domain/bank-broker/pack/repository"
)

//go:generate mockgen -source=service.go -destination=mocks/mock.go

type Application interface {
	CreateApplication(app bankbroker.ApplicationInput) error
	UpdateApplication(app bankbroker.ApplicationJobResponse) error
	GetApplicationById(id string) (bankbroker.Application, error)
	GetApplicationsByStatus(status string) ([]bankbroker.Application, error)
}

type Service struct {
	Application
}

func NewService(
	repo *repository.Repository, api *applapi.ApplicationApi, queue *applqueue.ApplicationQueue) *Service {
	return &Service{
		Application: NewApplicationService(repo.Application, api.Application, queue.Application),
	}
}
