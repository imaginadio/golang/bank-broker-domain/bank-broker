package service

import (
	bankbroker "gitlab.com/imaginadio/golang/bank-broker-domain/bank-broker"
	"gitlab.com/imaginadio/golang/bank-broker-domain/bank-broker/pack/communication/applapi"
	"gitlab.com/imaginadio/golang/bank-broker-domain/bank-broker/pack/communication/applqueue"
	"gitlab.com/imaginadio/golang/bank-broker-domain/bank-broker/pack/repository"
)

type ApplicationService struct {
	repo  repository.Application
	api   applapi.Application
	queue applqueue.Application
}

func NewApplicationService(repo repository.Application, api applapi.Application, queue applqueue.Application) *ApplicationService {
	return &ApplicationService{
		repo:  repo,
		api:   api,
		queue: queue,
	}
}

func (s *ApplicationService) CreateApplication(app bankbroker.ApplicationInput) error {
	// send http request to bank partner
	createdApp, err := s.api.CreateApplication(app)
	if err != nil {
		return err
	}
	// save result to db
	if err := s.repo.CreateApplication(createdApp); err != nil {
		return err
	}

	if createdApp.Status == "pending" {
		// send message to rabbitmq if status not final
		s.queue.SendPendingToQueue(createdApp.Id)
	}

	return nil
}

func (s *ApplicationService) UpdateApplication(app bankbroker.ApplicationJobResponse) error {
	return s.repo.UpdateApplication(app)
}

func (s *ApplicationService) GetApplicationById(id string) (bankbroker.Application, error) {
	return s.repo.GetApplicationById(id)
}

func (s *ApplicationService) GetApplicationsByStatus(status string) ([]bankbroker.Application, error) {
	return s.repo.GetApplicationsByStatus(status)
}
